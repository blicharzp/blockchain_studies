# Blockchain studies project

## Requirements 
`docker` 
`docker-compose` 

## How to run

### Docker Compose
To build images:
```
docker-compose build
```
To run peer-to-peer network:
```
docker-compose up --scale peer=<NUMBER>
```

## API
Once the P2P network is successfully launched, the API documentation for load balancer will be available on:
```
http://localhost:11000/docs
```