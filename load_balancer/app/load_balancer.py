import json

import requests
from fastapi import FastAPI, BackgroundTasks
from pydantic import BaseModel


class Peer(BaseModel):
    name: str
    port: int


app = FastAPI()

table = {"peers": {}, "count": 0}


@app.get("/")
async def get_table():
    return table


@app.post("/register")
async def register(peer: Peer, background_tasks: BackgroundTasks):
    table["peers"][peer.name] = peer.port
    table["count"] += 1
    background_tasks.add_task(_propagate)
    return "ok"


async def _propagate():
    for peer, port in table["peers"].items():
        requests.post(f"http://{peer}:{port}/update", data=json.dumps(table["peers"]))
    return "ok"


@app.get("/peer")
async def peer(name: str):
    response = requests.get(f"http://{name}:{table['peers'][name]}/")
    return response.json()
