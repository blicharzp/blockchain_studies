import json
import time
import requests
from typing import Dict

from fastapi import FastAPI, Body

from discover import discover


app = FastAPI()

table = {"self": {}, "others": {}}


@app.on_event("startup")
async def startup_event():
    name, port = discover()
    table["self"] = {name: port}
    time.sleep(1)
    requests.post("http://load_balancer:11000/register", data=json.dumps({"name": name, "port": port}))


@app.get("/")
async def get_table():
    return table


@app.post("/update")
async def update(peer_table: Dict[str, int] = Body(...)):
    for peer, port in peer_table.items():
        if peer not in table["self"]:
            table["others"][peer] = port
    return "ok"
