#! /bin/sh

export PORT=`python3 -c "from discover import discover_port; print(discover_port())"`;
uvicorn peer:app --host 0.0.0.0 --port ${PORT}