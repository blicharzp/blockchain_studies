import json
import os
import subprocess
from typing import Tuple


def discover_port() -> int:
    return discover()[1]


def discover() -> Tuple[str, int]:
    name, port = _get_service_metadata()
    return name, port


def _get_service_metadata() -> Tuple[str, int]:
    output = subprocess.check_output(
        ["curl", "-s", "--unix-socket", "/run/docker.sock", f"http://docker/containers/{os.environ['HOSTNAME']}/json"],
    )
    service_metadata = json.loads(output)
    for port in service_metadata["NetworkSettings"]["Ports"].values():
        host_port = int(port[0]["HostPort"])
    return (
        service_metadata["Name"][1:],
        host_port,
    )
